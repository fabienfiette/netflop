const request = require('supertest');
const { baseUrl } = require('./mock/globalVar');
const { getPricings } = require('./mock/pricings');

describe(`${baseUrl}/pricings/`, () => {
  describe(`GET pricings/ =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/pricings')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(getPricings));
    });
  });
});
