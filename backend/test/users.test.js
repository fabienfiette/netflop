const request = require('supertest');
const { getUser, externalTable, putUser } = require('./mock/users');
const { baseUrl } = require('./mock/globalVar');

describe(`${baseUrl}/users/`, () => {
  describe(`GET users/ =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/users')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });

  describe(`POST users/ =>`, () => {
    test("Insert a new user in table 'users'", async () => {
      const res = await request(baseUrl)
        .post('/users')
        .set('Accept', 'application/json')
        .send({
          email: "test@email.com",
          password: 'testPassword',
          firstName: 'Super',
          lastName: 'Test',
          address: 'Address',
          city: 'City',
          postalCode: '00000',
          pricing: 1
        });

      expect(res.statusCode).toBe(200);
      getUser[0].id_user = res.body.insertId;
      putUser[0].id_user = res.body.insertId;
    });
  });

  describe(`POST users/signin =>`, () => {
    test(`Verify avaible user with email and password`, async () => {
      const res = await request(baseUrl)
        .post(`/users/signin`)
        .set('Accept', 'application/json')
        .send({ email: "test@email.com", password: 'testPassword' });

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(getUser));
    });
  });

  describe(`GET users/user/:id =>`, () => {
    test(`Get a user with id`, async () => {
      const res = await request(baseUrl)
        .get(`/users/user/${getUser[0].id_user}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify([{ ...getUser[0], ...externalTable }]));
    });
  });

  describe(`GET users/view/:id =>`, () => {
    test(`Add 1 view to a user with id`, async () => {
      const res = await request(baseUrl)
        .get(`/users/view/${getUser[0].id_user}`)
        .set('Accept', 'application/json')

      getUser[0].viewed_movie++
      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(getUser));
    });
  });

  describe(`PUT users/update/:id =>`, () => {
    test(`Update user with specific body`, async () => {
      const res = await request(baseUrl)
        .put(`/users/update/${getUser[0].id_user}`)
        .set('Accept', 'application/json')
        .send({
          email: "test@email.com",
          password: 'password',
          firstName: 'Test',
          lastName: 'Super',
          address: 'address',
          city: 'city',
          postalCode: '00001',
          pricing: 1
        });

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(putUser));
    });
  });

  describe(`DELETE users/user/:id =>`, () => {
    test(`Delete a user with id`, async () => {
      const res = await request(baseUrl)
        .delete(`/users/user/${getUser[0].id_user}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(putUser));
    });
  });
});
