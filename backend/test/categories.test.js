const request = require('supertest');
const { baseUrl } = require('./mock/globalVar');

describe(`${baseUrl}/categories/`, () => {
  describe(`GET categories/ =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/categories')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });

  describe(`GET categories/:id =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/categories/1')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });
});
