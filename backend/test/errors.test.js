const request = require('supertest');
const { baseUrl } = require('./mock/globalVar');

describe(`${baseUrl}/users/`, () => {
  describe(`ERROR GET 404 =>`, () => {
    test("Should response the GET method with a status code 404", async () => {
      const res = await request(baseUrl)
        .get('/test-undefined-route')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(404);
      expect(res.text).toBe('Route not found');
    });
  });

  describe(`ERROR POST users/ =>`, () => {
    test("Send invalid field when insert a new user in table 'users'", async () => {
      const res = await request(baseUrl)
        .post('/users')
        .set('Accept', 'application/json')
        .send({});

      expect(res.statusCode).toBe(500);
      expect(res.text).toBe("{\"code\":\"ER_BAD_FIELD_ERROR\",\"errno\":1054,\"sqlMessage\":\"Unknown column 'NaN' in 'field list'\",\"sqlState\":\"42S22\",\"index\":0,\"sql\":\"INSERT INTO users (email, password,  first_name, last_name, adress, city, postal_code, id_pricing, id_image, viewed_movie) VALUES ('undefined','undefined','undefined','undefined','undefined','undefined','undefined',NaN, 1, 0);\"}");
    });
  });

  describe(`GET users/user/:id =>`, () => {
    test(`Get a user with id`, async () => {
      const res = await request(baseUrl)
        .get(`/users/user/NaN`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(500);
      expect(res.text).toBe("{\"code\":\"ER_BAD_FIELD_ERROR\",\"errno\":1054,\"sqlMessage\":\"Unknown column 'NaN' in 'where clause'\",\"sqlState\":\"42S22\",\"index\":0,\"sql\":\"SELECT u.*, i.*, p.* FROM users as u, images as i, pricings as p WHERE u.id_user = NaN AND u.id_pricing = p.id_pricing AND u.id_image = i.id_image;\"}");
    });
  });
});
