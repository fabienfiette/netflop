const request = require('supertest');
const { baseUrl } = require('./mock/globalVar');

describe(`${baseUrl}/actors/`, () => {
  describe(`GET actors/ =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/actors')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });

  describe(`GET actors/:id =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/actors/1')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });
});
