const deleteRow = {
  fieldCount: 0,
  affectedRows: 1,
  insertId: 0,
  serverStatus: 2,
  warningCount: 0,
  message: "",
  protocol41: true,
  changedRows: 0
}

const putRow = {
  fieldCount: 0,
  affectedRows: 1,
  insertId: 0,
  serverStatus: 2,
  warningCount: 0,
  message: "(Rows matched: 1  Changed: 1  Warnings: 0",
  protocol41: true,
  changedRows: 1
}
module.exports = {
  deleteRow,
  putRow
}
