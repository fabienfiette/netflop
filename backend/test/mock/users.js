const putUser = [{
  id_user: 0,
  email: "test@email.com",
  password: 'password',
  first_name: 'Test',
  last_name: 'Super',
  adress: 'address',
  city: 'city',
  postal_code: '00001',
  id_pricing: 1,
  id_image: 1,
  viewed_movie: 1
}]

const getUser = [{
  id_user: 0,
  email: "test@email.com",
  password: 'testPassword',
  first_name: 'Super',
  last_name: 'Test',
  adress: 'Address',
  city: 'City',
  postal_code: '00000',
  id_pricing: 1,
  id_image: 1,
  viewed_movie: 0
}]

const externalTable = {
  name: 'test_image',
  title: 'Free',
  price: 0
}

module.exports = {
  putUser,
  getUser,
  externalTable
}
