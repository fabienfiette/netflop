const getMovie = {
    id_movie: 1,
    title: "Heartstopper",
    rating: 0,
    description: "Teens Charlie and Nick discover their unlikely friendship might be something more as they navigate school and young love in this coming-of-age series.",
    start_date: 2022,
    minute_time: 30,
    child_protect: 1,
    video_url: "https://www.youtube.com/embed/gHALOizCpf0",
    id_categorie: 6,
    id_image: 3,
    upload_date: "2022-05-07T22:00:00.000Z",
    view: 3,
    cat_name: "Drama",
    name: "https://www.themoviedb.org/t/p/w300_and_h450_bestv2/wJJt1HG62h3WoGnLcRIbO2nNNkg.jpg"
}

const getFavMovie = {
    id_movie: 1,
    id_user: 13, 
    date_added: null,
    title: "Heartstopper",
    rating: 0,
    description: "Teens Charlie and Nick discover their unlikely friendship might be something more as they navigate school and young love in this coming-of-age series.",
    start_date: 2022,
    minute_time: 30,
    child_protect: 1,
    video_url: "https://www.youtube.com/embed/gHALOizCpf0",
    id_categorie: 6,
    id_image: 3,
    upload_date: "2022-05-07T22:00:00.000Z",
    view: 3,
    cat_name: "Drama",
    name: "https://www.themoviedb.org/t/p/w300_and_h450_bestv2/wJJt1HG62h3WoGnLcRIbO2nNNkg.jpg"
}

module.exports = {
    getMovie,
    getFavMovie
}
