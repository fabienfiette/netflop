const request = require('supertest');
const { baseUrl } = require('./mock/globalVar');
const { getMovie, getFavMovie } = require('./mock/movie');
const { putRow, deleteRow } = require('./mock/sqlResult');

describe(`${baseUrl}/movies/`, () => {
  describe(`GET movies/ =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/movies')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });

  describe(`GET movies/:id =>`, () => {
    test("Get a movie with id", async () => {
      const res = await request(baseUrl)
        .get(`/movies/movie/${getMovie.id_movie}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body[0])).toBe(JSON.stringify(getMovie));
    });
  });

  describe(`GET movies/top-view/ =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/movies/top-view')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });

  describe(`GET movies/recent-add/ =>`, () => {
    test("Should response the GET method with a status code 200", async () => {
      const res = await request(baseUrl)
        .get('/movies/recent-add')
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
    });
  });

  describe(`GET movies/search/:search =>`, () => {
    test("Search movie with title or categories", async () => {
      const res = await request(baseUrl)
        .get(`/movies/search/${getMovie.title}`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body[0])).toBe(JSON.stringify(getMovie));
    });
  });

  describe(`GET movies/view/:id =>`, () => {
    test(`Add 1 view to a movie with id`, async () => {
      const res = await request(baseUrl)
        .get(`/movies/view/6`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(putRow));
    });
  });

  describe(`POST movies/favorite/add =>`, () => {
    test(`Add favorite movie to a user`, async () => {
      const res = await request(baseUrl)
        .post(`/movies/favorite/add`)
        .set('Accept', 'application/json')
        .send({
          id_movie: 1,
          id_user: 13
        })

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(deleteRow));
    });
  });

  describe(`GET movies/favorite/:id =>`, () => {
    test(`Get favorite movies of user`, async () => {
      const res = await request(baseUrl)
        .get(`/movies/favorite/13`)
        .set('Accept', 'application/json')

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body[0])).toBe(JSON.stringify(getFavMovie));
    });
  });

  describe(`POST movies/favorite/delete =>`, () => {
    test(`Delete favorite movie of a user`, async () => {
      const res = await request(baseUrl)
        .post(`/movies/favorite/delete`)
        .set('Accept', 'application/json')
        .send({
          id_movie: 1,
          id_user: 13
        })

      expect(res.statusCode).toBe(200);
      expect(JSON.stringify(res.body)).toBe(JSON.stringify(deleteRow));
    });
  });
});
