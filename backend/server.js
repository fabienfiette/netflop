const express = require("express");
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const port = 3001;

const userRoute = require("./routes/users/index")
const pricingRoute = require("./routes/pricings/index")
const movieRoute = require("./routes/movies/index")
const categorieRoute = require("./routes/categories/index")
const actorRoute = require("./routes/actors/index")

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use("/users", userRoute);
app.use("/pricings", pricingRoute);
app.use("/movies", movieRoute);
app.use("/categories", categorieRoute);
app.use("/actors", actorRoute);

app.get('*', function (req, res) {
  res.status(404).send('Route not found');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
