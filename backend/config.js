const mysql = require("mysql");

const config = {
  dev: {
    host: 'localhost',
    user: 'root',
    password: 'mysqlRootPassword',
    database: 'netflop'
  },
  prod: {
    host: 'localhost',
    user: 'root',
    password: 'mysqlRootPassword',
    database: 'netflop'
  }
};

const connection = mysql.createConnection(config[process.argv[2] || 'dev']);

connection.connect(err => {
  if (err) throw err;
  console.log("Connected to mysql db")
});

module.exports = connection;
