const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.post("/favorite/delete", (req, res) => {
    mysqldb.query(`DELETE FROM favorite_movies WHERE id_movie = ${req.body.id_movie} AND id_user = ${req.body.id_user}`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

router.post("/favorite/add", (req, res) => {
    mysqldb.query(`INSERT INTO favorite_movies (id_movie, id_user) VALUES (${req.body.id_movie}, ${req.body.id_user})`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

module.exports = router;
