const express = require("express");
const router = express.Router();
const getMovie = require("./get")
const postMovie = require("./post")

router.use("/", getMovie);
router.use("/", postMovie);

module.exports = router;
