const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.get("/", (req, res) => {
    mysqldb.query(`SELECT * FROM movies as m, categories as c, images as i WHERE m.id_categorie = c.id_categorie AND m.id_image = i.id_image;`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

router.get("/movie/:id", (req, res) => {
    mysqldb.query(`SELECT * FROM movies as m, categories as c, images as i WHERE m.id_movie = ${req.params.id} AND m.id_categorie = c.id_categorie AND m.id_image = i.id_image;`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

router.get("/top-view", (req, res) => {
    mysqldb.query(`SELECT * FROM movies as m, categories as c, images as i WHERE m.id_categorie = c.id_categorie AND m.id_image = i.id_image ORDER BY view DESC LIMIT 5;`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

router.get("/recent-add", (req, res) => {
    mysqldb.query(`SELECT * FROM movies as m, categories as c, images as i WHERE m.id_categorie = c.id_categorie AND m.id_image = i.id_image ORDER BY m.upload_date ASC LIMIT 10;`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

router.get("/favorite/:id", (req, res) => {
    mysqldb.query(`SELECT * FROM favorite_movies as fm, movies as m, categories as c, images as i WHERE fm.id_movie = m.id_movie AND fm.id_user = ${req.params.id} AND m.id_categorie = c.id_categorie AND m.id_image = i.id_image ORDER BY date_added DESC;`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

router.get("/search/:search", (req, res) => {
    mysqldb.query(`SELECT * FROM movies as m, categories as c, images as i WHERE m.id_categorie = c.id_categorie AND m.id_image = i.id_image AND (m.title LIKE '%${req.params.search}%' OR c.cat_name LIKE '%${req.params.search}%');`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

router.get("/view/:id", (req, res) => {
    mysqldb.query(`UPDATE movies SET view = view + 1 WHERE id_movie = ${req.params.id};`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

module.exports = router;
