const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.get("/", (req, res) => {
  mysqldb.query(`SELECT * FROM pricings;`, (err, result) => {
    if (err) throw err;
    res.json(result);
  })
});

module.exports = router;
