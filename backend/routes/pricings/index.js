const express = require("express");
const router = express.Router();
const pricingRoute = require("./get")

router.use("/", pricingRoute);

module.exports = router;
