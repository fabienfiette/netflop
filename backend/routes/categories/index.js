const express = require("express");
const router = express.Router();
const getCategorie = require("./get")

router.use("/", getCategorie);

module.exports = router;
