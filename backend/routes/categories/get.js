const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.get("/", (req, res) => {
  mysqldb.query(`SELECT * FROM categories;`, (err, result) => {
    if (err) throw err;
    res.json(result);
  })
});

router.get("/:id", (req, res) => {
  mysqldb.query(`SELECT * FROM movies as m, categories as c, images as i WHERE m.id_categorie = c.id_categorie AND m.id_categorie = ${req.params.id} AND m.id_image = i.id_image LIMIT 10;`, (err, result) => {
    if (err) throw err;
    res.json(result);
  })
});

module.exports = router;
