const express = require("express");
const router = express.Router();
const deleteUser = require("./delete")
const getUser = require("./get")
const postUser = require("./post")
const updateUser = require("./update")

router.use("/", deleteUser);
router.use("/", getUser);
router.use("/", postUser);
router.use("/", updateUser);

module.exports = router;
