const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.get("/", (req, res) => {
  mysqldb.query(`SELECT u.*, i.*, p.* FROM users as u, images as i, pricings as p WHERE u.id_pricing = p.id_pricing AND u.id_image = i.id_image ORDER BY u.id_user;`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

router.get("/user/:id", (req, res) => {
  mysqldb.query(`SELECT u.*, i.*, p.* FROM users as u, images as i, pricings as p WHERE u.id_user = ${req.params.id} AND u.id_pricing = p.id_pricing AND u.id_image = i.id_image;`, (err, result) => {
    if (err) { res.status(500).send(err) };
    res.json(result);
  })
});

router.get("/view/:id", (req, res) => {
  mysqldb.query(`UPDATE users SET viewed_movie = viewed_movie + 1 WHERE id_user = ${req.params.id};`, (err, result) => {
    if (err) { res.status(500).send(err) };
    mysqldb.query(`SELECT u.* FROM users as u WHERE u.id_user = ${req.params.id};`, (err2, result2) => {
      if (err2) { res.send(err2) };
      res.json(result2);
    })
  })
});

module.exports = router;
