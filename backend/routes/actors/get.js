const express = require("express");
const router = express.Router();
const mysqldb = require("../../config");

router.get("/", (req, res) => {
    mysqldb.query(`SELECT * FROM actors;`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

router.get("/:id", (req, res) => {
    mysqldb.query(`SELECT a.* FROM movies_actors as ma, movies as m, actors as a WHERE ma.id_movie = ${req.params.id} AND ma.id_actor = a.id_actor GROUP BY a.id_actor;`, (err, result) => {
        if (err) throw err;
        res.json(result);
    })
});

module.exports = router;
