const express = require("express");
const router = express.Router();
const getActor = require("./get")

router.use("/", getActor);

module.exports = router;
