module.exports = {
  apps : [{
    name: "netflop-api",
    script: "./server.js",
    exec_mode: "fork",
    watch: true
  }]
}
