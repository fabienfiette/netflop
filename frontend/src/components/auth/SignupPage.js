import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { MenuItem, Select } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useAuth } from './authState';

export default function SignupPage() {
  const auth = useAuth();
  const location = useLocation();
  const navigate = useNavigate();
  const [pricings, setPricings] = useState(null);
  const [selectPricing, setSelectPricing] = useState(1);

  const from = location.state?.from?.pathname || '/'

  const handleSelect = (e) => {
    setSelectPricing(e.target.value);
  }

  useEffect(() => {
    if (auth.user) navigate(from, { replace: true })

    // Fetch pricings
    fetch(`${process.env.REACT_APP_BACKEND}/pricings`)
      .then((res) => res.json())
      .then((res) => {
        setPricings(res);
      });

  }, [auth.user, from, navigate])

  const handleSubmitLogin = (e) => {
    e.preventDefault();

    const data = new FormData(e.currentTarget);
    const formData = {
      lastName: data.get('last-name'),
      firstName: data.get('first-name'),
      email: data.get('email'),
      password: data.get('password'),
      city: data.get('city'),
      address: data.get('address'),
      postalCode: data.get('postal-code'),
      pricing: data.get('pricing')
    }

    auth.signup(formData, () => {
      navigate('/signin', { replace: true })
    })
  }

  return (
    <Container component="main" maxWidth="sm">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <Box component="form" onSubmit={handleSubmitLogin} noValidate sx={{ mt: 1 }}>
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <TextField
              margin="normal"
              required
              id="first-name"
              label="First Name"
              name="first-name"
              autoComplete="given-name"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              id="last-name"
              label="Last Name"
              name="last-name"
              autoComplete="family-name"
            />
          </Box>
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="new-password"
          />
          <TextField
            margin="normal"
            required
            fullWidth
            id="city"
            label="City"
            name="city"
            autoComplete="address-level2"
          />
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <TextField
              margin="normal"
              required
              id="address"
              label="Address"
              name="address"
              autoComplete="street-address"
            />
            <TextField
              margin="normal"
              required
              id="postal-code"
              label="Postal Code"
              name="postal-code"
              autoComplete="postal-code"
            />
          </Box>
          {pricings ? (
            <Select
              required
              fullWidth
              id="pricing"
              label="Pricing"
              name="pricing"
              value={selectPricing}
              onChange={handleSelect}
            >
              {pricings.map(obj => (
                <MenuItem key={obj.id_pricing} value={obj.id_pricing}>{`${obj.title} / ${obj.price}$`}</MenuItem>
              ))}
            </Select>) : (<></>)}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign Up
          </Button>
          <Grid container>
            <Grid item>
              <Typography variant="body2">
                {"Already have an account? "}
                <Link to="/signin" style={{ textDecoration: "underline" }}>
                  Sign In
                </Link>
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
}
