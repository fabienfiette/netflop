import { useState, useContext, createContext } from 'react';

const AuthContext = createContext();

export function AuthProvider({ children }) {
  const [user, setUser] = useState(null);
  const [favorite, setFavorite] = useState(null);

  const signin = (newUser) => {
    // Header
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    // Request option
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(newUser)
    };

    // Post
    fetch(`${process.env.REACT_APP_BACKEND}/users/signin`, requestOptions)
      .then(res => res.json())
      .then(res => res.length > 0 ? (() => { setUser(res[0]) })() : false)
  }

  const signup = (newUser, callback) => {
    // Header
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    // Request option
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(newUser)
    };

    // Post
    fetch(`${process.env.REACT_APP_BACKEND}/users`, requestOptions)
      .then(res => res.json())
      .then(res => res ? callback() : false)
      .catch(error => console.log('error', error));
  }

  const signout = callback => {
    return (() => {
      setUser(null);
      callback();
    })()
  }

  const update = async (newUser, callback) => {
    // Header
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    // Request option
    var requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: JSON.stringify(newUser)
    };

    // Post
    await fetch(`${process.env.REACT_APP_BACKEND}/users/update/${newUser.idUser}`, requestOptions)
      .then(res => res.json())
      .then(res => res ? callback() : false)
      .catch(error => console.log('error', error));
    // Fetch user
    fetch(`${process.env.REACT_APP_BACKEND}/users/user/${newUser.idUser}`)
      .then((res) => res.json())
      .then((res) => {
        setUser(res[0]);
      });
  }

  const getFavorite = async (userId) => {
    // Fetch favorite
    fetch(`${process.env.REACT_APP_BACKEND}/movies/favorite/${userId}`)
      .then((res) => res.json())
      .then((res) => {
        setFavorite(res);
      });
  }

  const value = { user, signin, signout, signup, update, favorite, getFavorite };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}

export function useAuth() {
  return useContext(AuthContext)
}
