import { Grid, Typography, Box, Paper } from '@mui/material';
import Container from '@mui/material/Container';
import { useEffect, useState } from 'react';
import { useAuth } from '../auth/authState';
import Movie from '../Movie/Movie';
import Categorie from './Categorie';

export default function Dashboard() {
  const auth = useAuth();
  const [categorie, setCategorie] = useState(null);
  const [favorite, setFavorite] = useState(null);
  const [userView, setUserView] = useState(null);

  useEffect(() => {
    // Fetch categories
    fetch(`${process.env.REACT_APP_BACKEND}/categories`)
      .then((res) => res.json())
      .then((res) => {
        setCategorie(res);
      })
    // Fetch favorite
    fetch(`${process.env.REACT_APP_BACKEND}/movies/favorite/${auth.user.id_user}`)
      .then((res) => res.json())
      .then((res) => {
        setFavorite(res);
      });
    // Fetch user view
    fetch(`${process.env.REACT_APP_BACKEND}/users/user/${auth.user.id_user}`)
      .then((res) => res.json())
      .then((res) => {
        setUserView(res[0].viewed_movie);
      });
  }, [auth.user.id_user])

  return (
    <Container fixed sx={{ marginBottom: 5 }}>

      {categorie ? categorie.map(obj => (
        <Box key={obj.id_categorie} sx={{ marginTop: 4 }}>
          <Typography gutterBottom variant="h4" component="h2">
            {obj.cat_name}
          </Typography>
          <Grid container spacing={2}>
            <Categorie state={obj} />
          </Grid>
        </Box>
      )) : (<></>)}

      <Box sx={{ marginTop: 4 }}>
        <Typography gutterBottom variant="h4" component="h2">
          Recent favorite
        </Typography>
        <Grid container spacing={2}>
          {favorite ? favorite.slice(0, 5).map(obj => (
            <Movie key={obj.id_movie} state={obj} />
          )) : <></>}
        </Grid>
      </Box>

      <Paper sx={{ marginTop: 4, padding: 3, textAlign: 'center' }} elevation={1}>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <Paper elevation={2} sx={{ padding: 1 }}>
              <Typography variant="h5" component="h6">
                Number of favorite movies :
              </Typography>
              <Typography variant="h5" component="h6">
                {favorite ? favorite.length : ""}
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper elevation={2} sx={{ padding: 1 }}>
              <Typography variant="h5" component="h6">
                Number of viewed movies :
              </Typography>
              <Typography variant="h5" component="h6">
                {userView}
              </Typography>
            </Paper>
          </Grid>
        </Grid>
        <Paper elevation={2} sx={{ marginTop: 2, padding: 2 }}>
          <Typography variant="h4" component="h2">
            {new Date().getDate()} / {new Date().getMonth() + 1} / {new Date().getFullYear()}
          </Typography>
        </Paper>
      </Paper>
    </Container >
  )
}
