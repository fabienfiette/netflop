import { useEffect, useState } from 'react';
import Movie from '../Movie/Movie';

export default function Categorie(props) {
  const [categorie, setCategorie] = useState(null);

  useEffect(() => {
    // Fetch categories
    fetch(`${process.env.REACT_APP_BACKEND}/categories/${props.state.id_categorie}`)
      .then((res) => res.json())
      .then((res) => {
        setCategorie(res);
      })
  }, [props.state.id_categorie])

  return (
    <>
      {
        categorie ? categorie.map(obj => (
          <Movie key={obj.id_movie} state={obj} />
        )) : (<></>)
      }
    </>
  )
}
