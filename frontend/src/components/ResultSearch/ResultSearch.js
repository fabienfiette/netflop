import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Movie from '../Movie/Movie';

export default function SignupPage() {
  const { search } = useParams();
  const [getSearch, setGetSearch] = useState(null);

  useEffect(() => {
    // Fetch movies
    fetch(`${process.env.REACT_APP_BACKEND}/movies/search/${search}`)
      .then((res) => res.json())
      .then((res) => {
        setGetSearch(res);
      });
  }, [search])

  return (
    <Container fixed>

      <Box sx={{ marginTop: 4 }}>
        <Typography gutterBottom variant="h4" component="h2">
          Result of search
        </Typography>
        <Grid container spacing={2}>
          {getSearch ? getSearch.map(obj => (
            <Movie key={obj.id_movie} state={obj} />
          )) : (<></>)}
        </Grid>
      </Box>

    </Container >
  );
}
