import { Grid, Card, CardContent, Typography, Box, CardHeader, CardActions, Button } from '@mui/material';
import Container from '@mui/material/Container';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Movie from '../Movie/Movie';

export default function Home() {
  const [topView, setTopView] = useState(null);
  const [recentAdd, setRecentAdd] = useState(null);
  const [pricings, setPricings] = useState(null);

  useEffect(() => {
    // Fetch top view
    fetch(`${process.env.REACT_APP_BACKEND}/movies/top-view`)
      .then((res) => res.json())
      .then((res) => {
        setTopView(res);
      });
    // Fetch recently added
    fetch(`${process.env.REACT_APP_BACKEND}/movies/recent-add`)
      .then((res) => res.json())
      .then((res) => {
        setRecentAdd(res);
      });
    // Fetch pricings
    fetch(`${process.env.REACT_APP_BACKEND}/pricings`)
      .then((res) => res.json())
      .then((res) => {
        setPricings(res);
      });
  }, [])

  return (
    <Container fixed>

      <Box sx={{ marginTop: 4 }}>
        <Typography gutterBottom variant="h4" component="h2">
          Top viewed movies
        </Typography>
        <Grid container spacing={2}>
          {topView ? topView.map(obj => (
            <Movie key={obj.id_movie} state={obj} />
          )) : (<></>)}
        </Grid>
      </Box>

      <Box sx={{ marginTop: 4 }}>
        <Typography gutterBottom variant="h4" component="h3">
          Recently added
        </Typography>
        <Grid container spacing={2}>
          {recentAdd ? recentAdd.map(obj => (
            <Movie key={obj.id_movie} state={obj} />
          )) : (<></>)}
        </Grid>
      </Box>

      <Box sx={{ textAlign: "center", marginTop: 5, height: "45vh" }}>
        <iframe width="85%" height="100%" src="https://www.youtube.com/embed/sY2djp46FeY?controls=0&rel=0" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
      </Box>

      <Box sx={{ marginTop: 5, marginBottom: 5 }}>

        <Container disableGutters maxWidth="sm" component="main" sx={{ pt: 5, pb: 4 }}>
          <Typography
            component="h2"
            variant="h3"
            align="center"
            gutterBottom
          >
            Pricing
          </Typography>
        </Container>

        <Container maxWidth="md" component="main">
          <Grid container spacing={5} alignItems="flex-end">
            {pricings ? pricings.map(obj => (
              <Grid
                key={obj.id_pricing}
                item
                xs={12}
                sm={obj.title === "Mounth" ? 12 : 6}
                md={4}
              >
                <Card>
                  <CardHeader
                    title={obj.title}
                    subheader={obj.title === "Year" ? "Most popular" : ""}
                    titleTypographyProps={{ align: 'center' }}
                    subheaderTypographyProps={{
                      align: 'center',
                    }}
                    sx={{
                      backgroundColor: (theme) =>
                        theme.palette.mode === 'light'
                          ? theme.palette.grey[200]
                          : theme.palette.grey[700],
                    }}
                  />
                  <CardContent>
                    <Box
                      sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'baseline'
                      }}
                    >
                      <Typography component="h2" variant="h3" color="text.primary">
                        ${obj.price}
                      </Typography>
                      <Typography variant="h6" color="text.secondary">
                        /{obj.title}
                      </Typography>
                    </Box>
                  </CardContent>
                  <CardActions>
                    <Link style={{ width: "100%" }} to="/signup">
                      <Button fullWidth variant={obj.title === "Year" ? "contained" : "outlined"}>
                        {obj.title === "Free" ? "Sign up for free" : "Get starded"}
                      </Button>
                    </Link>
                  </CardActions>
                </Card>
              </Grid>
            )) : (<></>)}
          </Grid>
        </Container>
      </Box>
    </Container >
  )
}
