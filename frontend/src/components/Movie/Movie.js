import { Grid, Card, CardActionArea, CardMedia, CardContent, Tooltip, Typography, Rating, CardActions, IconButton } from '@mui/material';
import { Favorite, FavoriteBorderOutlined } from '@mui/icons-material';
import { Link } from 'react-router-dom';
import { useAuth } from '../auth/authState';
import { useEffect, useState } from 'react';

export default function Movie(props) {
  const auth = useAuth();
  const [isFav, setIsFav] = useState(false);

  useEffect(() => {
    if (auth.favorite) {
      if (auth.favorite.filter(e => e.id_movie === props.state.id_movie).length > 0) {
        setIsFav(true)
      } else {
        setIsFav(false)
      }
    }
  }, [auth.favorite, props.state.id_movie])

  function removeFav() {
    // Header
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    // Request option
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify({ id_movie: props.state.id_movie, id_user: auth.user.id_user })
    };

    // Delete fav
    fetch(`${process.env.REACT_APP_BACKEND}/movies/favorite/delete`, requestOptions)
      .then((res) => res.json())
      .then(() => {
        auth.getFavorite(auth.user.id_user);
      });
  }

  function addFav() {
    // Header
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    // Request option
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify({ id_movie: props.state.id_movie, id_user: auth.user.id_user })
    };

    // Add fav
    fetch(`${process.env.REACT_APP_BACKEND}/movies/favorite/add`, requestOptions)
      .then((res) => res.json())
      .then(() => {
        auth.getFavorite(auth.user.id_user);
      });
  }

  function addView() {
    // Add view on user
    fetch(`${process.env.REACT_APP_BACKEND}/users/view/${auth.user.id_user}`)
    // Add view on 
    fetch(`${process.env.REACT_APP_BACKEND}/movies/view/${props.state.id_movie}`)
  }

  return (
    <Grid item xs={2.4}>
      <Card sx={{ width: '100%' }}>
        <Link to={`/movie/${props.state.id_movie}`}>
          <CardActionArea onClick={addView}>
            <CardMedia
              sx={{ height: { xs: 170, md: 250, lg: 300 } }}
              image={props.state.name}
              title={props.state.title}
            />
          </CardActionArea>
        </Link>
        <CardContent>
          <Tooltip title={props.state.title}>
            <Typography noWrap variant="h5" component="h2">
              {props.state.title}
            </Typography>
          </Tooltip>
          <Typography variant="body2" color="textSecondary" component="p">
            {props.state.cat_name} . {props.state.start_date}
          </Typography>
          <Rating name="read-only" value={props.state.rating} readOnly />
        </CardContent>
        {auth.favorite ? (
          <CardActions sx={{ padding: 0 }}>
            <IconButton aria-label="add to favorites" onClick={() => isFav ? removeFav() : addFav()}>
              {isFav ? <Favorite /> : <FavoriteBorderOutlined />}
            </IconButton>
          </CardActions>) : <></>}
      </Card>
    </Grid>
  )
}
