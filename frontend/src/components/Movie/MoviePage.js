import { Container, Grid, Paper, Rating, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"

export default function MoviePage() {
  const { id } = useParams();
  const [movie, setMovie] = useState(null);
  const [actors, setActors] = useState(null);

  useEffect(() => {
    // Fetch movie
    fetch(`${process.env.REACT_APP_BACKEND}/movies/movie/${id}`)
      .then((res) => res.json())
      .then((res) => {
        setMovie(res[0]);
      });
    // Fetch actors
    fetch(`${process.env.REACT_APP_BACKEND}/actors/${id}`)
      .then((res) => res.json())
      .then((res) => {
        setActors(res);
      })
  }, [id])

  return (
    <Container fixed>
      {movie ? (
        <Grid container spacing={3} sx={{ marginTop: 1 }}>
          <Grid item xs={6} md={4} lg={3}>
            <img width="100%" src={movie.name} alt={movie.title} />
          </Grid>
          <Grid item xs={12} md={8} lg={9}>
            <Typography variant="h5" component="h3">
              {movie.title} ({movie.start_date})
            </Typography>
            <Typography gutterBottom variant="subtitle1" color="text.secondary" component="p" sx={{ marginBottom: 2 }}>
              Details: {movie.cat_name} | {movie.child_protect ? "Child friendly" : "Not child friendly"} | {Math.floor(movie.minute_time / 60) + "h" + movie.minute_time % 60}
            </Typography>
            <Rating value={movie.rating} sx={{ marginBottom: 2 }} readOnly name="read-only" />
            <Typography variant="h6" component="h4">
              Overview
            </Typography>
            <Typography variant="subtitle1" color="text.secondary" component="p">
              {movie.description}
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <Typography variant="h6" component="h4">
              Actors
            </Typography>
            <Paper sx={{ padding: 1, textAlign: 'center', width: '100%', display: "flex", flexWrap: "wrap" }} elevation={1}>
              {actors ? actors.map(obj => (
                <Paper key={obj.id_actor} elevation={2} sx={{ padding: 1, marginLeft: 1 }}>
                  <Typography variant="h6" component="h6">
                    {obj.first_name} {obj.last_name}
                  </Typography>
                </Paper>
              )) : <></>}
            </Paper>
          </Grid>

          <Grid item xs={12} sx={{ textAlign: "center", height: "60vh", marginBottom: 5 }}>
            <iframe width="100%" height="100%" title={movie.title} src={`${movie.video_url}?controls=0&rel=0" title="YouTube video player`} frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
          </Grid>
        </Grid>
      ) : <></>}
    </Container>
  )
}
