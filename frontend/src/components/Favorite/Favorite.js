import { Grid, Typography, Box } from '@mui/material';
import Container from '@mui/material/Container';
import { useEffect, useState } from 'react';
import { useAuth } from '../auth/authState';
import Movie from '../Movie/Movie';

export default function Favorite() {
  const auth = useAuth();
  const [favorite, setFavorite] = useState(null);

  useEffect(() => {
    // Fetch favorite
    fetch(`${process.env.REACT_APP_BACKEND}/movies/favorite/${auth.user.id_user}`)
      .then((res) => res.json())
      .then((res) => {
        setFavorite(res);
      });
  }, [auth.user.id_user])

  return (
    <Container fixed>

      <Box sx={{ marginTop: 4 }}>
        <Typography gutterBottom variant="h4" component="h2">
          Favorite movies
        </Typography>
        <Grid container spacing={2}>
          {favorite ? favorite.map(obj => (
            <Movie key={obj.id_movie} state={obj} />
          )) : (<></>)}
        </Grid>
      </Box>

    </Container >
  )
}
