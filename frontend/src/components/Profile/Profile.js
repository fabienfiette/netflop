import { MenuItem, Select } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../auth/authState';

export default function SignupPage() {
  const auth = useAuth();
  const navigate = useNavigate();
  const [pricings, setPricings] = useState(null);
  const [selectPricing, setSelectPricing] = useState(1);
  const [getUser, setGetUser] = useState(null);

  const handleSelect = (e) => {
    setSelectPricing(e.target.value);
  }

  const handleInput = (e, target) => {
    setGetUser({...getUser, [target]: e.target.value})
  }

  useEffect(() => {
    // Fetch user
    fetch(`${process.env.REACT_APP_BACKEND}/users/user/${auth.user.id_user}`)
      .then((res) => res.json())
      .then((res) => {
        setGetUser(res[0]);
        setSelectPricing(res[0].id_pricing);
      });
    // Fetch pricings
    fetch(`${process.env.REACT_APP_BACKEND}/pricings`)
      .then((res) => res.json())
      .then((res) => {
        setPricings(res);
      });
  }, [auth.user.id_user])

  const handleSubmitLogin = (e) => {
    e.preventDefault();

    const data = new FormData(e.currentTarget);
    const formData = {
      idUser: auth.user.id_user,
      lastName: data.get('last-name'),
      firstName: data.get('first-name'),
      email: data.get('email'),
      password: data.get('password'),
      city: data.get('city'),
      address: data.get('address'),
      postalCode: data.get('postal-code'),
      pricing: data.get('pricing')
    }

    auth.update(formData, () => {
      navigate('/dashboard', { replace: true })
    })
  }

  return (
    <Container component="main" maxWidth="sm">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Typography component="h1" variant="h5">
          Update profile
        </Typography>
        {getUser ? (
          <Box component="form" onSubmit={handleSubmitLogin} noValidate sx={{ mt: 1 }}>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <TextField
                margin="normal"
                required
                id="first-name"
                label="First Name"
                name="first-name"
                autoComplete="given-name"
                autoFocus
                onChange={(e) => handleInput(e, "first_name")}
                value={getUser.first_name}
              />
              <TextField
                margin="normal"
                required
                id="last-name"
                label="Last Name"
                name="last-name"
                autoComplete="family-name"
                value={getUser.last_name}
              />
            </Box>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              value={getUser.email}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="new-password"
              value={getUser.password}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="city"
              label="City"
              name="city"
              autoComplete="address-level2"
              value={getUser.city}
            />
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
              <TextField
                margin="normal"
                required
                id="address"
                label="Address"
                name="address"
                autoComplete="street-address"
                value={getUser.adress}
              />
              <TextField
                margin="normal"
                required
                id="postal-code"
                label="Postal Code"
                name="postal-code"
                autoComplete="postal-code"
                value={getUser.postal_code}
              />
            </Box>
            {pricings ? (
              <Select
                required
                fullWidth
                id="pricing"
                label="Pricing"
                name="pricing"
                value={selectPricing}
                onChange={handleSelect}
              >
                {pricings.map(obj => (
                  <MenuItem key={obj.id_pricing} value={obj.id_pricing}>{`${obj.title} / ${obj.price}$`}</MenuItem>
                ))}
              </Select>) : (<></>)}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Update profile
            </Button>
          </Box>
        ) : <></>}
      </Box>
    </Container>
  );
}
