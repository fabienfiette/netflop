import { createTheme, CssBaseline, responsiveFontSizes, ThemeProvider } from '@mui/material';
import { Navigate, Route, Routes } from 'react-router-dom';
import './App.css';
import AppBar from './components/AppBar/AppBar';
import { AuthProvider } from './components/auth/authState';
import RequireAuth from './components/auth/RequireAuth';
import SigninPage from './components/auth/SigninPage';
import SignupPage from './components/auth/SignupPage';
import Home from './components/Home/Home';
import Favorite from './components/Favorite/Favorite';
import Dashboard from './components/Dashboard/Dashboard';
import MoviePage from './components/Movie/MoviePage';
import Profile from './components/Profile/Profile';
import ResultSearch from './components/ResultSearch/ResultSearch';

let theme = createTheme({
  palette: {
    mode: 'dark',
    dark: {
      main: '#121212'
    }
  }
})

theme = responsiveFontSizes(theme);

function App() {
  return (
    <AuthProvider>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AppBar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/dashboard' element={<RequireAuth><Dashboard /></RequireAuth>} />
          <Route path='/favorite' element={<RequireAuth><Favorite /></RequireAuth>} />
          <Route path='/profile' element={<RequireAuth><Profile /></RequireAuth>} />
          <Route path='/movie/:id' element={<RequireAuth><MoviePage /></RequireAuth>} />
          <Route path='/search/:search' element={<RequireAuth><ResultSearch /></RequireAuth>} />
          <Route path='/signin' element={<SigninPage />} />
          <Route path='/signup' element={<SignupPage />} />
          <Route path='*' element={<Navigate to="/" />} />
        </Routes>
      </ThemeProvider>
    </AuthProvider>
  );
}

export default App;
